FROM alpine:edge AS builder

ARG XINETD_VERSION=2.3.15.4

RUN apk update && apk add build-base musl-dev autoconf automake libtool pkgconf git libstdc++
RUN git clone -b ${XINETD_VERSION} https://github.com/openSUSE/xinetd.git
RUN cd xinetd && sh ./autogen.sh && ./configure && make && cd /
COPY server /server/
RUN cd /server && make rudicsd

FROM alpine:edge
EXPOSE 1023/tcp
RUN apk update && apk add --no-cache bash libstdc++ tcsh && \
    apk add --update --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing/ lrzsz
RUN mkdir -p /etc/xinetd.d/
COPY --from=builder /xinetd/xinetd /usr/sbin/
COPY --from=builder /server/rudicsd /usr/local/bin/
COPY xinetd.conf /etc/xinetd.conf
RUN addgroup iridium && \
    adduser -D -G iridium _iridium
RUN mkdir -p /home/_iridium/bin /home/_iridium/logs && \
    chown _iridium:iridium /home/_iridium/bin /home/_iridium/logs
COPY --chown=_iridium:iridium user /home/_iridium/
COPY init.sh /

CMD ["/init.sh"]
