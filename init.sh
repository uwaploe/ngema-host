#!/bin/bash
#
# Setup user account and start xinetd
#
: ${USER=emapex}

adduser -D -G iridium ${USER} -s /bin/tcsh
echo "${USER}:${PASSWORD}" | chpasswd

mkdir -p /home/$USER/logs
cp -a /home/_iridium/bin /home/$USER/

files=(.cshrc .szrc .rzrc)
for f in "${files[@]}"; do
    cp /home/_iridium/$f /home/$USER
done
chown -R $USER:iridium /home/$USER

exec /usr/sbin/xinetd -f /etc/xinetd.conf -dontfork -stayalive
