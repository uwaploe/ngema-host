# set the hostname
set hostname=`hostname`
# add directories for local commands
set path = (. ~/bin /bin /sbin /usr/sbin /usr/local/bin)
# set the prompt
set prompt=""$hostname":[$cwd]> "
